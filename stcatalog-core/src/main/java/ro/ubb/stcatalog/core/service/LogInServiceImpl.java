package ro.ubb.stcatalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.stcatalog.core.model.person.Administer;
import ro.ubb.stcatalog.core.model.person.Student;
import ro.ubb.stcatalog.core.model.person.Teacher;
import ro.ubb.stcatalog.core.model.person.Users;
import ro.ubb.stcatalog.core.repository.AdminRepository;
import ro.ubb.stcatalog.core.repository.LogInRepository;
import ro.ubb.stcatalog.core.repository.StudentRepository;
import ro.ubb.stcatalog.core.repository.TeacherRepository;
import ro.ubb.stcatalog.core.service.Interfaces.LogInService;

import java.util.Optional;


@Service
public class LogInServiceImpl implements LogInService{

    private static final Logger log = LoggerFactory.getLogger(LogInServiceImpl.class);

    @Qualifier("logInRepository")
    @Autowired
    private LogInRepository logInRepository;

    @Qualifier("adminRepository")
    @Autowired
    private AdminRepository adminRepository;

    @Qualifier("studentRepository")
    @Autowired
    private StudentRepository studentRepository;

    @Qualifier("teacherRepository")
    @Autowired
    private TeacherRepository teacherRepository;



    @Override
    @Transactional
    public Optional<Users> LogInStudent(String user, String pass){
        Optional<Users> s=logInRepository.findStudentLogin(user,pass);
        return s;
    }
    public Optional<Users> LogInTeacher(String user, String pass){
        Optional<Users> s=logInRepository.findTeacherLogin(user,pass);
        return s;
    }
    public Optional<Users> LogInCod(String user, String pass){
        Optional<Users> s=logInRepository.findCodLogin(user,pass);
        return s;
    }

    public Optional<Users> LonInAdmin(String user, String pass) {
        Optional<Users> s = logInRepository.findAdminLogin(user, pass);
        return s;
    }

    public Optional<Users> logIn(String user,String pass){
        Optional<Users> s=logInRepository.logIn(user,pass);
        return s;
    }

    public Administer getAdmin(Long id) {
        Administer administer = adminRepository.findOne(id);
        return administer;
    }

    public Student getStudent(Long id){
        Student s=studentRepository.findOne(id);
        return s;
    }

    public Teacher getTeacher(Long id){
        Teacher t=teacherRepository.findOne(id);
        return t;
    }

    public Teacher getCod(Long id){
        Teacher cod=teacherRepository.findCod(id);
        return cod;
    }
}
