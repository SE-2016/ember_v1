package ro.ubb.stcatalog.core.model.person;

import ro.ubb.stcatalog.core.model.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name ="Users")
public class Users extends BaseEntity<Long> {
    @Column(name = "userName")
    private String userName;
    @Column(name = "password")
    private String password;
    @OneToOne
    @JoinColumn(name = "student_id")
    private Student student;
    @OneToOne
    @JoinColumn(name = "teacher_id")
    private Teacher teacher;
    @OneToOne
    @JoinColumn(name = "admin_id")
    private Administer administer;
    @Column(name = "role")
    private String role;
    @OneToOne
    @JoinColumn(name = "ChiefOfDepartment_is")
    private ChiefOfDepartment chiefOfDepartment;

    public Users() {
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public ChiefOfDepartment getChiefOfDepartment() {
        return chiefOfDepartment;
    }

    public void setChiefOfDepartment(ChiefOfDepartment chiefOfDepartment) {
        this.chiefOfDepartment = chiefOfDepartment;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Administer getAdminister() {
        return administer;
    }

    public void setAdminister(Administer administer) {
        this.administer = administer;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


}
