package ro.ubb.stcatalog.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.ubb.stcatalog.core.model.person.Student;

/**
 * Created by radu.
 */


@Repository
public interface StudentRepository extends CatalogRepository<Student, Long> {
    @Override
    @Query("select s from Student s where s.id=:user_id")
    Student findOne(@Param("user_id") Long student_id);

}
