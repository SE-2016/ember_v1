package ro.ubb.stcatalog.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ro.ubb.stcatalog.core.model.person.Teacher;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="TeacherIn")

public class TeacherIn extends BaseEntity<Long>{
    @ManyToOne
    @JoinColumn(name = "teacher_id")
    @JsonIgnore
    private Teacher teacher;
    public TeacherIn(){
    }

    public TeacherIn(Long id1){
        super(id1);
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

}
