package ro.ubb.stcatalog.core.service.Interfaces;

import ro.ubb.stcatalog.core.model.OptionalDiscipline;
import ro.ubb.stcatalog.core.model.person.Teacher;

import java.util.List;
import java.util.Optional;

/**
 * Created by Rey on 4/18/2016.
 */
public interface TeacherService {
    List<Teacher> findAll();

    Optional<OptionalDiscipline> createOc(String name, String cod, long id, long semester_id);
}
