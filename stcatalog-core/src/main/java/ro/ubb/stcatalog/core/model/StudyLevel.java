package ro.ubb.stcatalog.core.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="StudyLevel")

public class StudyLevel extends BaseEntity<Long>{

    @Column(name="type")
    private String type;

    @OneToMany(mappedBy = "studyLevel")
    private List<StudyLine> studyLines;

    @ManyToOne
    @JoinColumn(name = "specialization_id")
    private Specialization specialization;

    public StudyLevel(){
    }
    public StudyLevel(Long id, String type){
        super(id);
        this.type=type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public StudyLine getStudyLineByType(String type) {
        for (StudyLine studyLine : this.studyLines) {
            if (studyLine.getType().equals(type))
                return studyLine;
        }
        return this.studyLines.get(0);
    }

    public List<StudyLine> getStudyLines() {
        return studyLines;
    }

    public void setStudyLines(List<StudyLine> studyLines) {
        this.studyLines = studyLines;
    }


    public Specialization getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Specialization specialization) {
        this.specialization = specialization;
    }


}
