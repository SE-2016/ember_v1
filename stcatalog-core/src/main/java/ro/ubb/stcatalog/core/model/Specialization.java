package ro.ubb.stcatalog.core.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Specialization")
public class Specialization extends BaseEntity<Long>{


    @Column(name="name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "faculty_id")
    private Faculty faculty;

    @OneToMany(mappedBy = "specialization")
    private List<StudyLevel> studyLevels;

    public Specialization(){
        studyLevels=new ArrayList<StudyLevel>();
    }
    public Specialization(Long id, String name){

        super(id);
        this.name=name;
        studyLevels=new ArrayList<StudyLevel>();
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public List<StudyLevel> getStudyLevels() {

        return studyLevels;
    }

    public void setStudyLevels(List<StudyLevel> studyLevels) {
        this.studyLevels = studyLevels;
    }

    public StudyLevel getStudyLevelByType(String type) {
        for (StudyLevel studyLevel : this.studyLevels) {
            if (studyLevel.getType().equals(type))
                return studyLevel;
        }
        return this.studyLevels.get(0);
    }

}
