package ro.ubb.stcatalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.stcatalog.core.model.OptionalDiscipline;
import ro.ubb.stcatalog.core.model.Semester;
import ro.ubb.stcatalog.core.model.person.Teacher;
import ro.ubb.stcatalog.core.repository.SemesterRepository;
import ro.ubb.stcatalog.core.repository.TeacherRepository;
import ro.ubb.stcatalog.core.repository.optionalDisciplineRepository;
import ro.ubb.stcatalog.core.service.Interfaces.TeacherService;

import java.util.List;
import java.util.Optional;

/**
 * Created by radu.
 */

@Service
public class TeacherServiceImpl implements TeacherService {

    private static final Logger log = LoggerFactory.getLogger(TeacherServiceImpl.class);

    @Qualifier("teacherRepository")
    @Autowired
    private TeacherRepository teacherRepository;

    @Qualifier("optionalDisciplineRepository")
    @Autowired
    private optionalDisciplineRepository optionalDisciplineRepository;

    @Qualifier("semesterRepository")
    @Autowired
    private SemesterRepository semesterRepository;

    @Override
    @Transactional
    public List<Teacher> findAll() {
        log.trace("findAll");

        List<Teacher> teachers= teacherRepository.findAll();

        log.trace("findAll: teachers={}", teachers);

        return teachers;
    }

    @Override
    @Transactional
    public Optional<OptionalDiscipline> createOc(String name, String code, long id, long semester_id) {

        int count = optionalDisciplineRepository.countProposed(id, semester_id);
        Semester semester = semesterRepository.findOne(semester_id);
        OptionalDiscipline opt = new OptionalDiscipline();
        opt.setName(name);
        opt.setCode(code);
        opt.setSemester(semester);
        opt.setTeacher(teacherRepository.findOne(id));
        if (count < 2) {
            opt = optionalDisciplineRepository.save(opt);
            return Optional.of(opt);
        }
        return Optional.empty();
    }
}
