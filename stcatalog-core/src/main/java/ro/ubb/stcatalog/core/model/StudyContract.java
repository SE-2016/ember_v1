package ro.ubb.stcatalog.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ro.ubb.stcatalog.core.model.person.Student;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="StudyContract")

public class StudyContract extends BaseEntity<Long> {

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "studyContract")
    @Fetch(value = FetchMode.SUBSELECT)
    List<Contract_OptDiscipline> contract_optDisciplines;

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(name = "contract_discipline",
            joinColumns = {@JoinColumn(name = "studyContract_id")},
            inverseJoinColumns = {@JoinColumn(name = "discipline_id")})
    private List<Discipline> disciplines;

    @OneToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn(name = "student_id")
    private Student student;

    public StudyContract(){

        disciplines = new ArrayList<Discipline>();
    }
    public StudyContract(Long id){
        super(id);

        disciplines = new ArrayList<Discipline>();
    }


    public List<Contract_OptDiscipline> getContract_optDisciplines() {
        return contract_optDisciplines;
    }

    public void setContract_optDisciplines(List<Contract_OptDiscipline> contract_optDisciplines) {
        this.contract_optDisciplines = contract_optDisciplines;
    }

    public List<Discipline> getDisciplines() {
        return disciplines;
    }

    public void setDisciplines(List<Discipline> disciplines) {
        this.disciplines = disciplines;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }




}


