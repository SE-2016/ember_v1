package ro.ubb.stcatalog.core.model;

import ro.ubb.stcatalog.core.model.person.Teacher;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ChiefOfDepartment")
public class ChiefOfDepartment extends Teacher {


    private static ChiefOfDepartment instance = null;
    protected ChiefOfDepartment() {
        // Exists only to defeat instantiation.
    }
    public static ChiefOfDepartment getInstance() {
        if(instance == null) {
            instance = new ChiefOfDepartment();
        }
        return instance;
    }

}
