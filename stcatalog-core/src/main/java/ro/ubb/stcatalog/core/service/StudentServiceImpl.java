package ro.ubb.stcatalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.stcatalog.core.model.Discipline;
import ro.ubb.stcatalog.core.model.StudyContract;
import ro.ubb.stcatalog.core.model.person.Student;
import ro.ubb.stcatalog.core.repository.StudentRepository;
import ro.ubb.stcatalog.core.service.Interfaces.StudentService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class StudentServiceImpl implements StudentService {

    private static final Logger log = LoggerFactory.getLogger(StudentServiceImpl.class);

    @Qualifier("studentRepository")
    @Autowired
    private StudentRepository studentRepository;

    @Override
    @Transactional
    public List<Student> findAll() {
        log.trace("findAll");

        List<Student> students = studentRepository.findAll();

        log.trace("findAll: students={}", students);

        return students;
    }

    @Override
    @Transactional
    public Student store(Student s) {
        log.trace("store");

        Student students = studentRepository.save(s);

        log.trace("student: store={}", students);

        return students;
    }
    @Override
    @Transactional
    public Optional<Student> LogIn(String user, String pass){
        //Optional<Student> s=studentRepository.findByUserNameAndUserPassword(user,pass);
        return null;
    }
    @Override
    @Transactional
    public List<Discipline> findAllDisciplines(Long studentId) {

        Student student = studentRepository.findOne(studentId);
        StudyContract studyContract = student.getStudyContract();
        List<Discipline> disciplines = studyContract.getDisciplines();
        return disciplines;
    }

    @Override
    @Transactional
    public List<Discipline> findAllDisciplinesByYear(Long studentId, Long year) {

        Student student = studentRepository.findOne(studentId);
        StudyContract studyContract = student.getStudyContract();
        List<Discipline> disciplines = studyContract.getDisciplines();
        List<Discipline> f1nal = new ArrayList<>();
        if (year == 1)
            f1nal = disciplines.stream()
                    .filter(p -> p.getSemester().getNumber() < 3).collect(Collectors.toList());
        if (year == 2)
            f1nal = disciplines.stream()
                    .filter(p -> p.getSemester().getNumber() > 2 && p.getSemester().getNumber() < 5).collect(Collectors.toList());
        if (year == 3)
            f1nal = disciplines.stream()
                    .filter(p -> p.getSemester().getNumber() > 4 && p.getSemester().getNumber() < 7).collect(Collectors.toList());
        if (year == 0)
            f1nal = disciplines;
        return f1nal;
    }
}
