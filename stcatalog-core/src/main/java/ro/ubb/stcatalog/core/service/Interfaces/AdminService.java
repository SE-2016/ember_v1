package ro.ubb.stcatalog.core.service.Interfaces;

import ro.ubb.stcatalog.core.model.person.Student;

/**
 * Created by Ileni on 06/05/2016.
 */

public interface AdminService {
    Student createStudent(String firstName, String lastName, String serialNumber, String specialization, String studyLevel, String studyLine, int semester, int group, String username, String password);
}
