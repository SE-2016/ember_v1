package ro.ubb.stcatalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.stcatalog.core.model.Faculty;
import ro.ubb.stcatalog.core.repository.FacultyRepository;
import ro.ubb.stcatalog.core.service.Interfaces.FacultyService;

import java.util.List;


@Service
public class FacultyServiceImpl implements FacultyService {

    private static final Logger log = LoggerFactory.getLogger(FacultyServiceImpl.class);

    @Qualifier("facultyRepository")
    @Autowired
    private FacultyRepository facultyRepository;

    @Override
    @Transactional
    public List<Faculty> findAll() {
        log.trace("findAll");

        List<Faculty> faculties= facultyRepository.findAll();

        log.trace("findAll: faculties={}", faculties);

        return faculties;
    }
}
