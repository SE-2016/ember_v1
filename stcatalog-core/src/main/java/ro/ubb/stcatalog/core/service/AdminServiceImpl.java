package ro.ubb.stcatalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.stcatalog.core.model.*;
import ro.ubb.stcatalog.core.model.person.Student;
import ro.ubb.stcatalog.core.model.person.Users;
import ro.ubb.stcatalog.core.repository.AdminRepository;
import ro.ubb.stcatalog.core.repository.LogInRepository;
import ro.ubb.stcatalog.core.repository.StudentRepository;
import ro.ubb.stcatalog.core.service.Interfaces.AdminService;

import java.util.Optional;

/**
 * Created by Ileni on 06/05/2016.
 */

@Service
public class AdminServiceImpl implements AdminService {

    private static final Logger log = LoggerFactory.getLogger(AdminServiceImpl.class);

    @Qualifier("adminRepository")
    @Autowired
    private AdminRepository adminRepository;

    @Qualifier("studentRepository")
    @Autowired
    private StudentRepository studentRepository;

    @Qualifier("loginRepository")
    @Autowired
    private LogInRepository logInRepository;

    @Override
    @Transactional
    public Student createStudent(String firstName, String lastName, String serialNumber,
                                 String specialization, String studyLevel, String studyLine, int semester,
                                 int group, String username, String password) {

        String defCnp = "123";
        Student student = new Student(firstName, lastName, defCnp, serialNumber);

        //set the group
        Optional<Specialization> specialization1 = adminRepository.findSpecializationByName(specialization);
        Specialization specialization2 = specialization1.get();
        StudyLevel studyLevel1 = specialization2.getStudyLevelByType(studyLevel);
        StudyLine studyLine1 = studyLevel1.getStudyLineByType(studyLine);
        Semester semester1 = studyLine1.getSemesterByNumber(semester);
        Group group1 = semester1.getGroupByNumber(group);
        student.setGroup(group1);


        Student student1 = studentRepository.save(student);

        Users user1 = new Users();
        user1.setPassword(password);
        user1.setUserName(username);
        user1.setStudent(student1);
        logInRepository.save(user1);
        log.trace("createStudent: student={}", student1);

        return student1;
    }
}
