package ro.ubb.stcatalog.core.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Faculty")
public class Faculty extends BaseEntity<Long>{

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="name")
    private String name;

    public List<Specialization> getSpecializations() {
        return specializations;
    }

    public void setSpecializations(List<Specialization> specializations) {
        this.specializations = specializations;
    }


    @JsonManagedReference
    @OneToMany(mappedBy = "faculty",cascade = CascadeType.ALL)
    private List<Specialization> specializations;
    public Faculty(){
        specializations=new ArrayList<Specialization>();
        departments=new ArrayList<Department>();
    }
    public Faculty(Long id, String name){
        super(id);
        this.name=name;
        specializations=new ArrayList<Specialization>();
        departments=new ArrayList<Department>();
    }
    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

    @JsonManagedReference
    @OneToMany(mappedBy = "faculty")
    private List<Department> departments;

}
