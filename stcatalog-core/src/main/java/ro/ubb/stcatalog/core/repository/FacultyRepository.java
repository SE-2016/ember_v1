package ro.ubb.stcatalog.core.repository;

import org.springframework.stereotype.Repository;
import ro.ubb.stcatalog.core.model.Faculty;

@Repository
public interface FacultyRepository extends CatalogRepository<Faculty, Long> {

}