package ro.ubb.stcatalog.core.service.Interfaces;

import ro.ubb.stcatalog.core.model.Faculty;

import java.util.List;


public interface FacultyService {
    List<Faculty> findAll();
}
