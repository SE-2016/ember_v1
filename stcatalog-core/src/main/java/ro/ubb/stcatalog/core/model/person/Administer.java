package ro.ubb.stcatalog.core.model.person;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Ileni on 06/05/2016.
 */

@Entity
@Table(name = "administer")
public class Administer extends Person {

    @Column(name = "rank")
    String rank;

    public Administer() {
    }

    public Administer(String firstName, String lastName, String cnp) {
        super(firstName, lastName, cnp);
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }
}