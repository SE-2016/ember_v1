package ro.ubb.stcatalog.core.model;

import javax.persistence.*;

@Entity
@Table(name = "Contract_OptDiscipline")

public class Contract_OptDiscipline extends BaseEntity<Long> {

    @ManyToOne
    @JoinColumn(name = "optionalDiscipline_id")
    private OptionalDiscipline optionalDiscipline;

    @ManyToOne
    @JoinColumn(name = "studyContract_id")
    private StudyContract studyContract;

    @Column(name = "priority")
    private int priority;

    public OptionalDiscipline getOptionalDiscipline() {
        return optionalDiscipline;
    }

    public void setOptionalDiscipline(OptionalDiscipline optionalDiscipline) {
        this.optionalDiscipline = optionalDiscipline;
    }

    public StudyContract getStudyContract() {
        return studyContract;
    }

    public void setStudyContract(StudyContract studyContract) {
        this.studyContract = studyContract;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }


}
