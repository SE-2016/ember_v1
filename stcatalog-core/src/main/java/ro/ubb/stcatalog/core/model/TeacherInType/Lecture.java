package ro.ubb.stcatalog.core.model.TeacherInType;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import ro.ubb.stcatalog.core.model.Discipline;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name="Lecture")
public class Lecture extends TeacherIn {


    @OneToMany(mappedBy = "lecture")
    @JsonManagedReference

    private List<Discipline> disciplines;
    public Lecture(){

    }

    public List<Discipline> getDisciplines() {
        return disciplines;
    }

    public void setDisciplines(List<Discipline> disciplines) {
        this.disciplines = disciplines;
    }


}
