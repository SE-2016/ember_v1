package ro.ubb.stcatalog.core.repository;

import org.springframework.stereotype.Repository;
import ro.ubb.stcatalog.core.model.Semester;

@Repository
public interface SemesterRepository extends CatalogRepository<Semester, Long> {
}
