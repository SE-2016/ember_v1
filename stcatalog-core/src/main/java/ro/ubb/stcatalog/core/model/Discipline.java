package ro.ubb.stcatalog.core.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import ro.ubb.stcatalog.core.model.TeacherInType.Laboratory;
import ro.ubb.stcatalog.core.model.TeacherInType.Lecture;
import ro.ubb.stcatalog.core.model.TeacherInType.Seminary;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Discipline")
public class Discipline extends BaseEntity<Long> {

    @ManyToMany(mappedBy = "disciplines")
    List<StudyContract> studyContracts;
    @Column(name = "code")
    private String code;
    @Column(name = "name")
    private String name;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "semester_id")
    private Semester semester;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "exam_id")
    private Exam exam;
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "lecture_id")
    private Lecture lecture;
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "seminary_id")
    private Seminary seminary;
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "laboratoryid")
    private Laboratory laboratory;

    public Discipline() {

    }

    public Discipline(Long id, String code, String name) {
            super(id);
            this.code = code;
            this.name = name;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



        public Lecture getLecture() {
            return lecture;
        }

        public void setLecture(Lecture lecture) {
            this.lecture = lecture;
        }

        public Seminary getSeminary() {
            return seminary;
        }

        public void setSeminary(Seminary seminary) {
            this.seminary = seminary;
        }

        public Laboratory getLaboratory() {
            return laboratory;
        }

        public void setLaboratory(Laboratory laboratory) {
            this.laboratory = laboratory;
        }

    public List<StudyContract> getStudyContracts() {
        return studyContracts;
    }

    public void setStudyContracts(List<StudyContract> studyContracts) {
        this.studyContracts = studyContracts;
    }
}
