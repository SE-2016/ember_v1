package ro.ubb.stcatalog.core.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.ubb.stcatalog.core.model.Specialization;
import ro.ubb.stcatalog.core.model.person.Administer;

import java.util.Optional;

/**
 * Created by Ileni on 06/05/2016.
 */

@Repository
public interface AdminRepository extends CatalogRepository<Administer, Long> {

    @Query("select s from Specialization s where s.name=:specialization")
    Optional<Specialization> findSpecializationByName(@Param("specialization") String specialization);
}
