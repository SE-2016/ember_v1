package ro.ubb.stcatalog.core.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import ro.ubb.stcatalog.core.model.person.Teacher;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Department")

public class Department extends BaseEntity<Long>{
    @Column(name = "name")
    private String name;
    @ManyToOne
    @JoinColumn(name = "faculty_id")
    @JsonBackReference
    private Faculty faculty;
    @JsonManagedReference
    @OneToMany(mappedBy = "department")
    private List<Teacher> teachers;
    @JsonManagedReference
    @OneToMany(mappedBy = "department")
    private List<OptionalDiscipline> optionalDisciplines;

    public Department() {
    }

    public Department(Long id, String name) {
        super(id);
        this.name = name;
        optionalDisciplines = new ArrayList<OptionalDiscipline>();
        teachers = new ArrayList<Teacher>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }

}
