package ro.ubb.stcatalog.core.model.person;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ro.ubb.stcatalog.core.model.Exam;
import ro.ubb.stcatalog.core.model.Group;
import ro.ubb.stcatalog.core.model.StudyContract;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Student")
public class Student extends Person {

    @Column(name = "serialNumber")
    private String serialNumber;

    @ManyToOne
    @JoinColumn(name = "group_id")
    private Group group;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "student")
    @JsonIgnore
    private List<Exam> exams;

    @OneToOne(mappedBy = "student")
    @JsonIgnore
    private StudyContract studyContract;

    public Student(){

    }
    public Student(Long id, String firstName,String lastName,String cnp){
        super(id,firstName,lastName,cnp);
        exams = new ArrayList<Exam>();
    }
    public Student(Long id, String firstName,String lastName,String cnp,String serialNumber){
        super(id,firstName,lastName,cnp);
        this.serialNumber = serialNumber;
        exams = new ArrayList<Exam>();
    }
    public Student(String firstName,String lastName,String cnp,String serialNumber){
        super(firstName,lastName,cnp);
        this.serialNumber = serialNumber;
        exams = new ArrayList<Exam>();
    }

    public Student(String firstName, String lastName, String cnp, String serialNumber, Group group, StudyContract studyContract) {
        super(firstName, lastName, cnp);
        this.serialNumber = serialNumber;
        this.group = group;
        this.studyContract = studyContract;
        exams = new ArrayList<Exam>();
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public StudyContract getStudyContract() {
        return studyContract;
    }

    public void setStudyContract(StudyContract studyContract) {
        this.studyContract = studyContract;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public List<Exam> getExams() {
        return exams;
    }

    public void setExams(List<Exam> exams) {
        this.exams = exams;
    }

}
