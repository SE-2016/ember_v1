import Ember from 'ember';

export default Ember.Component.extend({

  authManager: Ember.inject.service('session'),
  sessionAccount:Ember.inject.service('sessionAccount'),
  actions: {
    authenticate() {
      const { login, password } = this.getProperties('login', 'password');
        this.get('authManager').authenticate('authenticator:oauth2', login, password).then(() => {
          Ember.set(this.get('authManager'),'username', login), 
          Ember.set(this.get('authManager'),'password', password),

          alert('Success!');
        });
    },
    invalidateSession() {
            this.get('authManager').invalidate();
        }
  }
});