import Ember from 'ember';

export default Ember.Component.extend({
  store: Ember.inject.service(''),
	authManager: Ember.inject.service('session'),
  sessionAccount:Ember.inject.service('sessionAccount'),
  actions: {
  	login:function() {
      this.sendAction('onLogin');
    },
    invalidateSession: function() {
            this.get('authManager').invalidate();
        }  

  },
});
