import DS from 'ember-data';
import Ember from 'ember';
export default DS.Model.extend({
  name: DS.attr(),

  toString: Ember.computed('name',function() {
    return `${this.get('name')}`;
  })
});

