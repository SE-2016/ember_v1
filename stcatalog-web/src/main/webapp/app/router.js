import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('login');

  this.route('teacher', function () {
    this.route('OC');
    this.route('courses');
  });

  this.route('cod',{path:'/'},function() {
    this.route('allteachers',{path:'/allteachers'});
    this.route('courses');
    this.route('optionalcourses');
    this.route('withbestresults');
  });
  this.route('student', function() {
    this.route('course');
    this.route('courseYear1');
    this.route('courseYear2');
    this.route('courseYear3');

    this.route('table', function () {
      this.route('courses');
    });
  });


  this.route('administrator', function () {
    this.route('add-student');
  });
});

export default Router;
