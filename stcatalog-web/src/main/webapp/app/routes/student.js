import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import Ember from 'ember';
export default Ember.Route.extend(AuthenticatedRouteMixin, {
	store: Ember.inject.service(''),
	model(){
		if (this.get('isAuthenticated')) {
			this.get('store').queryRecord('user', {}).then((user) => {
        	this.set('currentUser', user),
        	alert(this.get("username"));
      	});
		}
	}
});