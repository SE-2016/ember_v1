import ESASession from "ember-simple-auth/services/session";
import Ember from 'ember';

export default ESASession.extend({
  store: Ember.inject.service(''),
  session:Ember.inject.service('session'),
    setCurrentUser: function() {
    if (this.get('isAuthenticated')) {
        this.set('currentUser', "user");
    }
  }.observes('isAuthenticated')
});
