package ro.ubb.stcatalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.ubb.stcatalog.core.model.Discipline;
import ro.ubb.stcatalog.core.model.person.Student;
import ro.ubb.stcatalog.core.service.Interfaces.StudentService;
import ro.ubb.stcatalog.web.dto.DisciplinesDto;
import ro.ubb.stcatalog.web.dto.StudentsDto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
public class StudentController {

    private static final Logger log = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/students", method = RequestMethod.GET, produces = "application/vnd.api+json")
    public
    @ResponseBody
    StudentsDto getStudents() {
        log.trace("getStudents");

        List<Student> students = studentService.findAll();

        log.trace("getStudents: students={}", students);

        return new StudentsDto(students);
    }

    @RequestMapping(value = "/students/save", method = RequestMethod.GET, produces = "application/vnd.api+json")
    public
    @ResponseBody
    StudentsDto save(Student s) {
        log.trace("save");

        Student students = studentService.store(s);

        log.trace("save: students={}", students);
        List<Student> l1st=new ArrayList<>();
        l1st.add(students);
        return new StudentsDto(l1st);
    }


    @RequestMapping(value = "/disciplines", params = {"student_id", "year"}, method = RequestMethod.GET, produces = "application/vnd.api+json", consumes = "application/vnd.api+json")
    public
    @ResponseBody
    DisciplinesDto getDisciplineOfStudentByYear(@RequestParam(value = "student_id") final Long studentId, @RequestParam(value = "year") final Long year) {
        log.trace("getDsiciplines");

        List<Discipline> dsiciplines = studentService.findAllDisciplinesByYear(studentId, year);
        Collections.sort(dsiciplines, (o1, o2) -> o1.getSemester().getNumber() - o2.getSemester().getNumber());
        log.trace("getdsiciplines: dsiciplines={}", dsiciplines);

        return new DisciplinesDto(dsiciplines);
    }
}
