package ro.ubb.stcatalog.web.dto;

import ro.ubb.stcatalog.core.model.person.Teacher;

import java.util.List;


public class TeachersDto {
    private List<Teacher> teachers;

    public TeachersDto() {
    }

    public TeachersDto(List<Teacher> teachers) {
        this.teachers = teachers;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }


}
