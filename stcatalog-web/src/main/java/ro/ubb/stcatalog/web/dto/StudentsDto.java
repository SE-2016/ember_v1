package ro.ubb.stcatalog.web.dto;

import ro.ubb.stcatalog.core.model.person.Student;

import java.util.List;


public class StudentsDto {
    private List<Student> students;

    public StudentsDto() {
    }

    public StudentsDto(List<Student> students) {
        this.students = students;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }


}
