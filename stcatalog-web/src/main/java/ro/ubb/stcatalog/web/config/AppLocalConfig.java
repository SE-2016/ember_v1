package ro.ubb.stcatalog.web.config;

import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import ro.ubb.stcatalog.core.JPAConfig;

/**
 * Local configuration activated by the local Spring profile.
 */
@Configuration
@ComponentScan("ro.ubb.stcatalog.core")
@Import({JPAConfig.class})
@Profile("local")
@PropertySources({@PropertySource(value = "classpath:local/db.properties"),
})
public class AppLocalConfig {

    /**
     * Enables placeholders usage with SpEL expressions.
     *
     * @return
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
