package ro.ubb.stcatalog.web.dto;

import ro.ubb.stcatalog.core.model.person.Teacher;

import java.util.List;

/**
 * Created by radu.
 */
public class TeacherDto {
    private List<Teacher> teachers;

    public TeacherDto() {
    }

    public TeacherDto(List<Teacher> teachers) {
        this.teachers= teachers;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Teacher> teachers) {
        this.teachers= teachers;
    }


}
