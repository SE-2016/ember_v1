package ro.ubb.stcatalog.web.dto;

import ro.ubb.stcatalog.core.model.OptionalDiscipline;


public class OptionalDisciplineDto {
    private OptionalDiscipline opt;

    public OptionalDisciplineDto() {
    }

    public OptionalDisciplineDto(OptionalDiscipline oc) {
        this.opt = oc;
    }

    public OptionalDiscipline getOpt() {
        return opt;
    }

    public void setOpt(OptionalDiscipline opt) {
        this.opt = opt;
    }

    public String getDisciplineGroup() {
        return opt.getDisciplineGroup();
    }

    public void setDisciplineGroup(String disciplineGroup) {
        this.setDisciplineGroup(disciplineGroup);
    }

    public String getName() {
        return opt.getName();
    }

    public void setName(String name) {
        opt.setName(name);
    }

    public String getCode() {
        return opt.getCode();
    }

    public void setCode(String code) {
        opt.setCode(code);
    }

}
