package ro.ubb.stcatalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ro.ubb.stcatalog.core.model.person.Student;
import ro.ubb.stcatalog.core.service.Interfaces.AdminService;
import ro.ubb.stcatalog.web.dto.StudentDto;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ileni on 07/05/2016.
 */
@RestController
public class AdminController {

    private static final Logger log = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private AdminService adminService;

    @RequestMapping(value = "/onAddStudents", method = RequestMethod.POST, produces = "application/vnd.api+json")
    public Map<String, StudentDto> createOnAddStudent(final HttpServletRequest request) throws IOException {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String serialNumber = request.getParameter("serialNumber");
        String specialization = request.getParameter("specialization");
        String studyLevel = request.getParameter("studyLevel");
        String studyLine = request.getParameter("studyLine");
        int semester = Integer.parseInt(request.getParameter("semester"));
        int group = Integer.parseInt(request.getParameter("group"));
        String username = request.getParameter("username");
        String password = request.getParameter("password");


        Student student = adminService.createStudent(firstName, lastName, serialNumber, specialization, studyLevel, studyLine, semester, group, username, password);

        Map<String, StudentDto> studentDtoMap = new HashMap<>();
        studentDtoMap.put("student", new StudentDto(student));

        log.trace("createStudent: studentDtoMap={}", studentDtoMap);

        return studentDtoMap;
    }

}
