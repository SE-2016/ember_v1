package ro.ubb.stcatalog.web.dto;

import ro.ubb.stcatalog.core.model.Discipline;

public class DisciplineDto {
    private long id;
    private String code;
    private String name;
    private int semester;
    private String lecture;
    private String seminary;
    private String laboratory;

    public DisciplineDto() {
    }

    public DisciplineDto(Discipline discipline) {
        this.id = discipline.getId();
        this.code = discipline.getCode();
        this.name = discipline.getName();
        this.semester = discipline.getSemester().getNumber();

        this.laboratory = discipline.getLaboratory().getTeacher().getFirstName();
        this.lecture = discipline.getLecture().getTeacher().getFirstName();
        this.seminary = discipline.getSeminary().getTeacher().getFirstName();
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public String getLaboratory() {
        return laboratory;
    }

    public void setLaboratory(String laboratory) {
        this.laboratory = laboratory;
    }

    public String getSeminary() {
        return seminary;
    }

    public void setSeminary(String seminary) {
        this.seminary = seminary;
    }

    public String getLecture() {
        return lecture;
    }

    public void setLecture(String lecture) {
        this.lecture = lecture;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
