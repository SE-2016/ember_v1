
package ro.ubb.stcatalog.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ro.ubb.stcatalog.core.model.person.Administer;
import ro.ubb.stcatalog.core.model.person.Student;
import ro.ubb.stcatalog.core.model.person.Teacher;
import ro.ubb.stcatalog.core.model.person.Users;
import ro.ubb.stcatalog.core.service.Interfaces.LogInService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

@RestController
public class LoginController{

    private static final Logger log = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private LogInService logInService;


    @RequestMapping(value = "/token", method = RequestMethod.POST,produces = "application/vnd.api+json")
    public void tokenStudent(final HttpServletRequest request,
                      final HttpServletResponse response) throws IOException {
        String user=request.getParameter("username");
        String pass=request.getParameter("password");

        Optional<Users> s=logInService.logIn(user,pass);
        if(s.isPresent()) {
            final HttpSession session = request.getSession();
            session.setAttribute("acces_token",
                    "token");
            session.setAttribute("requestTokenSecret",
                    "tokenSecret");
            JSONObject json = new JSONObject();
            ObjectMapper mapper = new ObjectMapper();
            String role=s.get().getRole();
            JSONObject j2=new JSONObject();
            if (role.equals("admin")) {
                Administer admin = s.get().getAdminister();
                j2.put("id", admin.getId());
                j2.put("cnp", admin.getCnp());
                j2.put("firstName", admin.getFirstName());
                j2.put("lastName", admin.getLastName());
                j2.put("rank", admin.getRank());
                json.put("access_token", "admin bs");
                json.put("admin", j2);
            }
            if(role.equals("student")){
                Student student=s.get().getStudent();
                j2.put("id",student.getId());
                j2.put("cnp",student.getCnp());
                j2.put("firstName",student.getFirstName());
                j2.put("lastName",student.getLastName());
                j2.put("serialNumber",student.getSerialNumber());
                json.put("access_token", "student bs");
                json.put("student",j2);
            }
            if (role.equals("teacher")){
                Teacher teacher=s.get().getTeacher();
                j2.put("id", teacher.getId());
                j2.put("cnp",teacher.getCnp());
                j2.put("firstName",teacher.getFirstName());
                j2.put("lastName",teacher.getLastName());
                j2.put("rank",teacher.getRank());
                json.put("access_token", "teacher bs");
                json.put("teacher",j2);
            }
            if(role.equals("cod")){
                Teacher teacher=logInService.getCod(s.get().getChiefOfDepartment().getId());
                j2.put("id", teacher.getId());
                j2.put("cnp",teacher.getCnp());
                j2.put("firstName",teacher.getFirstName());
                j2.put("lastName",teacher.getLastName());
                j2.put("rank",teacher.getRank());
                json.put("access_token", "cod bs");
                json.put("teacher",j2);
            }
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(json.toString());
        }
        else
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid user");
        // Redirect to the authorized URL page and retrieve the verifier code.

    }


    @RequestMapping(value = "/users", method = RequestMethod.GET,produces = "application/vnd.api+json")
    public @ResponseBody void users(final HttpServletResponse response) throws IOException {
        JSONObject json = new JSONObject();
        JSONObject json1 = new JSONObject();
        json1.put("firstName","Ileni Tudor");
        json1.put("id",1);
        json.put("user",json1);
        response.setContentType("application/json");
        response.getWriter().write(json.toString());
    }
}


