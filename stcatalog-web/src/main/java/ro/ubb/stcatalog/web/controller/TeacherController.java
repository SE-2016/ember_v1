package ro.ubb.stcatalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ro.ubb.stcatalog.core.model.OptionalDiscipline;
import ro.ubb.stcatalog.core.model.person.Teacher;
import ro.ubb.stcatalog.core.service.Interfaces.TeacherService;
import ro.ubb.stcatalog.web.dto.OptionalDisciplineDto;
import ro.ubb.stcatalog.web.dto.TeacherDto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by radu.
 */


@RestController
public class TeacherController {

    private static final Logger log = LoggerFactory.getLogger(TeacherController.class);

    @Autowired
    private TeacherService teacherService;

    @RequestMapping(value = "/teachers", method = RequestMethod.GET, produces = "application/vnd.api+json")
    public
    @ResponseBody
    TeacherDto getTeachers() {
        log.trace("getTeachers");

        List<Teacher> teachers = teacherService.findAll();

        log.trace("getTeachers: teachers={}", teachers);

        return new TeacherDto(teachers);
    }

    @RequestMapping(value = "/optionalDisciplines", method = RequestMethod.POST, produces = "application/vnd.api+json")
    public void createOC(final HttpServletRequest request,
                         final HttpServletResponse response) throws IOException {
        String name1 = request.getParameter("name");
        String cod = request.getParameter("cod");
        Long id = Long.valueOf(request.getParameter("teacher_id"));
        Long semester_id = Long.valueOf(request.getParameter("semester_id"));

        Optional<OptionalDiscipline> oc = teacherService.createOc(name1, cod, id, semester_id);
        if (oc.isPresent()) {
            Map<String, OptionalDisciplineDto> OdDtoMap = new HashMap<>();
            OdDtoMap.put("optionalDiscipline", new OptionalDisciplineDto(oc.get()));

            response.getWriter().write(String.valueOf(OdDtoMap));
        } else
            response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Already 2 disciplines sugested");
    }
}