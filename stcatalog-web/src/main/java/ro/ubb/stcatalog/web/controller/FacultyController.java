package ro.ubb.stcatalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ro.ubb.stcatalog.core.model.Faculty;
import ro.ubb.stcatalog.core.service.Interfaces.FacultyService;
import ro.ubb.stcatalog.web.dto.FacultyDto;

import java.util.List;



@RestController
public class FacultyController {

    private static final Logger log = LoggerFactory.getLogger(FacultyController.class);

    @Autowired
    private FacultyService facultyService;

    @RequestMapping(value = "/faculties", method = RequestMethod.GET, produces = "application/vnd.api+json")
    public
    @ResponseBody
    FacultyDto getFaculties() {
        log.trace("getFaculties");

        List<Faculty> faculties = facultyService.findAll();

        log.trace("getFaculties: faculties={}", faculties);

        return new FacultyDto(faculties);
    }
}
